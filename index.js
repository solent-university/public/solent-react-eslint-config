module.exports = {
    plugins: ['react'],
    extends: ['@solent/eslint-config'],
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
            modules: true,
            experimentalObjectRestSpread: true
        }
    },
    rules: {
        /* react plugin rules */
        'react/jsx-wrap-multilines': [
            2,
            {
                declaration: 'parens-new-line',
                assignment: 'parens-new-line',
                return: 'parens-new-line',
                arrow: 'parens-new-line',
                condition: 'parens-new-line',
                logical: 'parens-new-line',
                prop: 'ignore'
            }
        ],
        'react/display-name': 0,
        'react/require-default-props': 0,
        'react/no-this-in-sfc': 2,
        'react/no-unsafe': 1,
        'react/no-unused-prop-types': 1,
        'react/no-unused-state': 1,
        'react/no-will-update-set-state': 2,
        'react/self-closing-comp': 2,
        'react/sort-comp': [
            1,
            {
                order: [
                    'static-variables',
                    'instance-variables',
                    'constructor',
                    'static-methods',
                    'lifecycle',
                    'everything-else',
                    'render'
                ]
            }
        ],
        'react/sort-prop-types': 1,
        'react/style-prop-object': 2,
        'react/jsx-curly-spacing': [2, { when: 'never', children: true }],
        'react/prop-types': [
            2,
            { ignore: ['children', 'history', 'theme', 'location', 'className'] }
        ],
        'react/jsx-tag-spacing': [
            2,
            {
                beforeSelfClosing: 'always',
                beforeClosing: 'never'
            }
        ],
        'react/jsx-closing-bracket-location': 2,
        'react/jsx-no-bind': [1, { allowArrowFunctions: true }],
        'react/jsx-pascal-case': 2
    },
    settings: {
        react: {
            version: '^16.0.0'
        }
    }
};
