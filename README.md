# Solent eslint config - with React

Configuration for ESLint within linting settings for React using [eslint-plugin-react](https://www.npmjs.com/package/eslint-plugin-react). This configuration extends our base eslint configuration [@solent/eslint-config](https://www.npmjs.com/package/@solent/eslint-config).

## Peer dependencies

- [eslint](https://www.npmjs.com/package/eslint)
- [eslint-plugin-react](https://www.npmjs.com/package/eslint-plugin-react)